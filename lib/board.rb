# In your Board class, you should have a grid instance variable to keep track of the board tiles.
# You should also have the following methods:
#
#     place_mark, which takes a position such as [0, 0] and a mark such as :X as arguments.
# It should throw an error if the position isn't empty.
#     empty?, which takes a position as an argument
#     winner, which should return a mark
#     over?, which should return true or false
#     If you want to be a little fancy, read the Bracket Methods reading.

class Board
  attr_reader :grid
  MARKS = [:X, :O]

  def initialize(grid = [[nil,nil,nil], [nil,nil,nil], [nil,nil,nil]])
    @grid = grid
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def empty?(pos)
    pos1 = pos[0]
    pos2 = pos[1]
    @grid[pos1][pos2].nil?
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark unless !(empty?(pos))
  end

  def winner
    if @grid[0][0] == :X && @grid[0][1] == :X && @grid[0][2] == :X
      :X
    elsif @grid[1][0] == :X && @grid[1][1] == :X && @grid[1][2] == :X
      :X
    elsif @grid[2][0] == :X && @grid[2][1] == :X && @grid[2][2] == :X
      :X
    elsif @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X
      :X
    elsif @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X
      :X
    elsif @grid[0][0] == :O && @grid[1][0] == :O && @grid[2][0] == :O
      :O
    elsif @grid[0][1] == :O && @grid[1][1] == :O && @grid[2][1] == :O
      :O
    elsif @grid[0][2] == :O && @grid[1][2] == :O && @grid[2][2] == :O
      :O
    end
  end

  def over?
    grid.flatten.none? { |pos| pos.nil? } || winner
  end

end
